import asyncio
import logging
import paramiko
import sshtunnel

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "[%(asctime)s] %(name)s:%(lineno)d => %(message)s", "%Y-%m-%d %H:%M:%S"
)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


async def run(service, input_values: dict) -> None:
    ryax_local_port = input_values.get("ryax_endpoint_port")

    # Reading pkey from file
    logger.info("Creating ssh key from file...")
    pkey = paramiko.RSAKey.from_private_key_file(input_values.get("ssh_pkey"))

    logger.info("Starting server connection...")

    # Discover the private node
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(
        input_values.get("ssh_host"),
        input_values.get("ssh_port"),
        username=input_values.get("ssh_user"),
        pkey=pkey
    )
    cmd='squeue -n jupyter -o "%R" -h'
    stdin, stdout, stderr = client.exec_command(cmd)
    stdout = stdout.readlines()
    client.close()
    for line in stdout:
        running_node = line.strip()
    logger.info(f"Found node {running_node}")

    logger.info("Start serving...")
    try:
        client = paramiko.SSHClient()
        with sshtunnel.open_tunnel(
            (input_values.get("ssh_host"), input_values.get("ssh_port")),
            ssh_username=input_values.get("ssh_user"),
            ssh_pkey=pkey,
            remote_bind_address=(running_node, input_values.get("remote_port")),
            local_bind_address=('0.0.0.0', ryax_local_port)
        ):
                client.load_system_host_keys()
                client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                while True:
                    try:
                        client.connect('127.0.0.1', ryax_local_port)
                    except (TimeoutError, paramiko.ssh_exception.SSHException):
                        logger.warn("Timeout occurred, this is possibly due to the double hop, ignoring...")
                        pass
    except Exception as err:
        logger.error("Finished serving, cause:")
        logger.error(err)
    finally:
        logger.info("Finishing connection")
        client.close()
    logger.info('Finished!')


if __name__ == "__main__":
    input_json = {
        "ssh_pkey": "secret",
        "ssh_user": "secret",
        "ssh_host": "secret",
        "ssh_port": "secret",
        "remote_port": "secret",
        "ryax_endpoint_protocol": "http",
        "ryax_endpoint_instance_name": "localhost",
        "ryax_endpoint_prefix": "/",
        "ryax_endpoint_port": 8080,
    }

    # Populate with secrets for testing
    import json

    with open("../secrets.txt") as f:
        secrets = json.load(f)
        for key in secrets:
            if key in input_json:
                input_json[key] = secrets[key]

    from unittest.mock import AsyncMock

    asyncio.run(run(AsyncMock(), input_json))
